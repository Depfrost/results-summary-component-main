# Frontend Mentor - Results summary component solution

This is a solution to the [Results summary component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/results-summary-component-CE_K6s0maV). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page

### Screenshot

![](./assets/images/screenshot-desktop.png)
![](./assets/images/screenshot-mobile.png)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### What I learned

Better management of hsl colors in css via custom properties

Variables fonts

aspect-ratio css property for the circle with gradient

### Continued development

In future projects I would like to practice more with CSS and learn more about best practices when developping a responsive website.
I also would like to use tailwind and practice with animations.

### Useful resources

- [Youtube challenge solution](https://www.youtube.com/watch?v=KqFAs5d3Yl8) - This helped me to improve and compare my code to another solution for this challenge.

## Author

- Frontend Mentor - [@Depfrost](https://www.frontendmentor.io/profile/depfrost)
